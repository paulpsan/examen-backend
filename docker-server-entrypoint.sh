#!/bin/bash
echo "bootstraping server"
cd /usr/src/app
POSTGRES_UP=1000
echo "waiting for postgres to startup"
while [ $POSTGRES_UP != 0 ]; do
  sleep 1
  echo "testing is postgres is up"
  if [ -z "$DB_PASSWORD" ]
  then
    psql -h $DB_HOST -p $DB_PORT -U $DB_USERNAME
  else
    echo "enter credentials"
    PGPASSWORD=$DB_PASSWORD psql -h $DB_HOST -p $DB_PORT -U $DB_USERNAME
  fi
  POSTGRES_UP=$?
  echo "exit code $POSTGRES_UP"
done
echo "postgres started"
if [ $NODE_ENV == "test" ] || [ $NODE_ENV == "dev" ]
then
  URI_DB="$DB_DIALECT://$DB_USERNAME:$DB_PASSWORD@$DB_HOST:$DB_PORT/$DB_DATABASE"
  node_modules/.bin/sequelize db:migrate --url $URI_DB
  node_modules/.bin/sequelize db:seed:undo:all --url $URI_DB
  node_modules/.bin/sequelize db:seed:all --url $URI_DB
  if [ $NODE_ENV == "test" ]
  then
    echo "Starting environment testing"
    npm run test
  else
    echo "Starting environment developing"
    npm run start
  fi
elif [ $NODE_ENV == "prod" ] 
then
  echo "Starting environment production"
  npm run start
else
  echo "Environment invalid"
fi