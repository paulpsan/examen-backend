FROM node:12.16.3-alpine
WORKDIR /usr/src/app
RUN apk update
RUN apk add bash python postgresql-client g++ make
COPY package*.json ./
COPY . .
VOLUME /usr/src/app/reports
RUN npm install
RUN cp docker-server-entrypoint.sh /usr/local/bin/ && \
  chmod +x /usr/local/bin/docker-server-entrypoint.sh
ENTRYPOINT [ "/usr/local/bin/docker-server-entrypoint.sh" ]